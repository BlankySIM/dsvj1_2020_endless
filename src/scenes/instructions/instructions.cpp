#include "instructions.h"

namespace runner
{
	namespace instructions
	{
		const int minusc = 96;
		const int mayusc = 32;
		int key;

		void init()
		{

		}
		void inputs()
		{
			key = GetKeyPressed();

			if (key > minusc)
			{
				key -= mayusc;
			}
		}
		void update()
		{
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);
			switch (key)
			{
			case KEY_M:
				screens::currentScreen = screens::MenuScreen;
				PlaySound(audio::buttonSfx);
				break;
			}
		}
		void draw()
		{
			sprites::drawBackground();
			DrawTexture(sprites::shortButton, static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenHeight) - 56, WHITE);
			DrawText("Instructions", static_cast<int>(screens::middleScreenW) - 125, static_cast<int>(screens::screenZero) + 10, screens::maxFontSize, WHITE);
			DrawText("M  Menu", static_cast<int>(screens::screenZero) + 28, static_cast<int>(screens::screenHeight) - 42, screens::minFontSize, WHITE);
			DrawText("Rules", static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenZero) + 60, screens::medFontSize, WHITE);
			DrawText("Run endlessly while trying to avoid enemies and catch coins", static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenZero) + 100, screens::minFontSize, WHITE);
			DrawText("Enemies", static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenZero) + 130, screens::medFontSize, WHITE);
			DrawTexture(sprites::obstacle, static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenZero) + (sprites::obstacle.height / 2 + 160), WHITE);
			DrawText("This enemy can't be eliminated, but you can jump over it.", static_cast<int>(screens::screenZero) + (sprites::obstacle.width + 7), static_cast<int>(screens::screenZero) + (sprites::obstacle.height / 2 + 160), screens::minFontSize, WHITE);
			DrawTexture(sprites::weakObstacle, static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenZero) + (sprites::weakObstacle.height / 2 + 220), WHITE);
			DrawText("You can eliminate this enemy using your attack and earn 1 point.", static_cast<int>(screens::screenZero) + (sprites::weakObstacle.width + 7), static_cast<int>(screens::screenZero) + (sprites::weakObstacle.height / 2 + 220), screens::minFontSize, WHITE);
			DrawTexture(sprites::bigObstacle, static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenZero) + (sprites::bigObstacle.height / 2 + 280), WHITE);
			DrawText("You can't eliminate nor jump this enemy, try changing between rails.", static_cast<int>(screens::screenZero) + (sprites::bigObstacle.width + 9), static_cast<int>(screens::screenZero) + (sprites::bigObstacle.height / 2 + 280), screens::minFontSize, WHITE);
		}
		void deinit()
		{

		}
	}
}