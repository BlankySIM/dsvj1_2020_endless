#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "audio_manager/audio.h"
#include "sprites_manager/sprites.h"

namespace runner
{
	namespace instructions
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif