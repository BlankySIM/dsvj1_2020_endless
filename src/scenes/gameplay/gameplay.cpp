#include "gameplay.h"

namespace runner
{
	namespace gameplay
	{
		bool pause = false;
		bool goToMenu = false;
		bool started = false;
		const int debugConsoleW = 170;
		const int debugConsoleH = 60;
		const int hardModeLimit = 200;
		bool hardMode = false;

		static void setHardMode()
		{
			if (!hardMode && player::runnerPlayer.points > hardModeLimit)
			{
				obstacle::maxCooldown = obstacle::hardMaxCooldown;
				hardMode = true;
			}
		}
		void init()
		{
			player::init();
			coin::init();
			obstacle::init();
		}
		static void pauseInputs()
		{
			if (IsKeyPressed(KEY_P))
			{
				PlaySound(audio::buttonSfx);
				if (pause)
				{
					pause = false;
				}
				else
				{
					pause = true;
				}
			}
			if (pause && IsKeyPressed(KEY_M))
			{
				goToMenu = true;
			}
		}
		void inputs()
		{
			player::inputs();
			if (IsKeyPressed(KEY_SPACE))
			{
				started = true;
			}
			pauseInputs();
			if (pause)
			{
				audio::muteInput();
			}
			#if DEBUG
			if (IsKeyPressed(KEY_R))
			{
				PlaySound(audio::buttonSfx);
				player::init();
				coin::init();
				obstacle::init();
				started = false;
				pause = false;
				obstacle::maxCooldown = obstacle::easyMaxCooldown;
				hardMode = false;
			}
			#endif
		}
		void update()
		{
			PlayMusicStream(audio::gameplayMusic);
			UpdateMusicStream(audio::gameplayMusic);
			setHardMode();

			if (!pause)
			{
				if (!audio::muted)
				{
					SetMusicVolume(audio::gameplayMusic, audio::gameplayMusicVolume);
				}
				player::update();
				if (started)
				{
					coin::generateCoins();
					coin::moveCoins();
					obstacle::generateObstacles();
					obstacle::moveObstacles();
					sprites::updateParallax();
				}
				if (!player::runnerPlayer.alive)
				{
					screens::currentScreen = screens::ResultsScreen;
					obstacle::maxCooldown = obstacle::easyMaxCooldown;
					hardMode = false;
					if (!audio::muted)
					{
						SetMusicVolume(audio::gameplayMusic, audio::pauseVolume);
					}
					started = false;
				}
			}
			else
			{
				if (!audio::muted)
				{
					SetMusicVolume(audio::gameplayMusic, audio::pauseVolume);
				}
				if (goToMenu)
				{
					PlaySound(audio::buttonSfx);
					player::init();
					coin::init();
					obstacle::init();
					pause = false;
					goToMenu = false;
					screens::currentScreen = screens::MenuScreen;
					StopMusicStream(audio::gameplayMusic);
					started = false;
					obstacle::maxCooldown = obstacle::easyMaxCooldown;
					hardMode = false;
				}
			}
		}
		static void drawPause()
		{
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero), static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenHeight), Fade(BLACK, 0.8f));
			DrawTexture(sprites::longButton, static_cast<int>(screens::middleScreenW) - sprites::longButton.width / 2, static_cast<int>(screens::middleScreenH) - 39, WHITE);
			DrawTexture(sprites::longButton, static_cast<int>(screens::middleScreenW) - sprites::longButton.width / 2, static_cast<int>(screens::middleScreenH) + 36, WHITE);
			DrawText("Pause", static_cast<int>(screens::middleScreenW) - 60, static_cast<int>(screens::screenZero) + 80, screens::maxFontSize, WHITE);
			DrawText("M  Go to menu", static_cast<int>(screens::middleScreenW) - 105, static_cast<int>(screens::middleScreenH) - 25, screens::medFontSize, WHITE);
			DrawText("P   Continue", static_cast<int>(screens::middleScreenW) - 103, static_cast<int>(screens::middleScreenH) + 50, screens::medFontSize, WHITE);
			DrawText("Press ESC to exit", static_cast<int>(screens::screenWidth) - 200, static_cast<int>(screens::screenHeight) - 30, screens::minFontSize, WHITE);
			DrawText("Q/A -> Up/Down || K -> Jump || L -> Attack", static_cast<int>(screens::screenZero) + 15, static_cast<int>(screens::screenHeight) - 30, screens::minFontSize, WHITE);
			audio::drawMuteButton(static_cast<int>(screens::middleScreenW) - (sprites::shortButton.width / 2), static_cast<int>(screens::middleScreenH) + 190);
		}
		static void drawPlayerControls()
		{
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero), static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenHeight), Fade(BLACK, 0.7f));
			DrawTexture(sprites::shortButton, static_cast<int>(player::runnerPlayer.collisionRec.x - 20), static_cast<int>(player::runnerPlayer.collisionRec.y - 105), WHITE);
			DrawTexture(sprites::shortButton, static_cast<int>(player::runnerPlayer.collisionRec.x - 20), static_cast<int>(player::runnerPlayer.collisionRec.y - 55), WHITE);
			DrawTexture(sprites::shortButton, static_cast<int>((player::runnerPlayer.collisionRec.x - 20) + (sprites::shortButton.width + 5)), static_cast<int>(player::runnerPlayer.collisionRec.y - 55), WHITE);
			DrawTexture(sprites::shortButton, static_cast<int>((player::runnerPlayer.collisionRec.x - 20) + (sprites::shortButton.width + 5)), static_cast<int>(player::runnerPlayer.collisionRec.y - 105), WHITE);
			DrawText("Q     Up", static_cast<int>(player::runnerPlayer.collisionRec.x + 2), static_cast<int>(player::runnerPlayer.collisionRec.y - 92), screens::minFontSize, WHITE);
			DrawText("A    Down", static_cast<int>(player::runnerPlayer.collisionRec.x + 2), static_cast<int>(player::runnerPlayer.collisionRec.y - 42), screens::minFontSize, WHITE);
			DrawText("K  Jump", static_cast<int>((player::runnerPlayer.collisionRec.x) + (sprites::shortButton.width + 7)), static_cast<int>(player::runnerPlayer.collisionRec.y - 92), screens::minFontSize, WHITE);
			DrawText("L Attack", static_cast<int>((player::runnerPlayer.collisionRec.x) + (sprites::shortButton.width + 8)), static_cast<int>(player::runnerPlayer.collisionRec.y - 42), screens::minFontSize, WHITE);
			DrawText("PRESS SPACE", static_cast<int>(screens::middleScreenW + 20), static_cast<int>(screens::middleScreenH - screens::maxFontSize), screens::maxFontSize, WHITE);
			DrawText(" TO START", static_cast<int>(screens::middleScreenW + 32), static_cast<int>(screens::middleScreenH + 5), screens::maxFontSize, WHITE);
		}
		void draw()
		{
			sprites::drawParallaxBackground();
			sprites::drawPlatforms();
			if (!started)
			{
				drawPlayerControls();
			}
			player::draw();
			coin::draw();
			obstacle::draw();			
			if (pause)
			{
				drawPause();
			}
			else
			{
				DrawTexture(sprites::shortButton, static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenZero) + 60, WHITE);
				DrawText("P   Pause", static_cast<int>(screens::screenZero) + 29, static_cast<int>(screens::screenZero) + 74, screens::minFontSize, WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenHeight) - 50, debugConsoleW, debugConsoleH, Fade(BLACK, 0.8f));
				DrawText("R -> Restart", static_cast<int>(screens::screenZero) + 20, static_cast<int>(screens::screenHeight) - 30, screens::minFontSize, WHITE);
				#endif
			}
		}
		void deinit()
		{

		}
	}
}