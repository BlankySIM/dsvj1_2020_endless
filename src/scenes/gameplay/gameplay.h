#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "raylib.h"
#include "player/player.h"
#include "coin/coin.h"
#include "obstacle/obstacle.h"
#include "sprites_manager/sprites.h"
#include "audio_manager/audio.h"

namespace runner
{
	namespace gameplay
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif


