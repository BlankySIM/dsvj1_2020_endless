#include "menu.h"

namespace runner
{
	namespace menu
	{
		const int minusc = 96;
		const int mayusc = 32;
		int key;

		void init()
		{
			sprites::init(screens::screenWidth, screens::screenHeight);
			sprites::initParallaxPositions();
			audio::setVolumes();
		}
		void inputs()
		{	
			audio::muteInput();
			key = GetKeyPressed();

			if (key > minusc)
			{
				key -= mayusc;
			}
		}
		void update()
		{
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);

			switch (key)
			{
			case KEY_ONE:
				screens::currentScreen = screens::GameplayScreen;
				StopMusicStream(audio::menuMusic);
				PlaySound(audio::buttonSfx);
				break;
			case KEY_TWO:
				screens::currentScreen = screens::InstructionsScreen;
				PlaySound(audio::buttonSfx);
				break;
			case KEY_THREE:
				screens::currentScreen = screens::CreditsScreen;
				PlaySound(audio::buttonSfx);
				break;
			}
		}
		void draw()
		{
			sprites::drawBackground();			
			DrawTexture(sprites::longButton, static_cast<int>(screens::middleScreenW) - sprites::longButton.width/2, static_cast<int>(screens::middleScreenH) - 70, WHITE);
			DrawTexture(sprites::longButton, static_cast<int>(screens::middleScreenW) - sprites::longButton.width / 2, static_cast<int>(screens::middleScreenH) + 21, WHITE);
			DrawTexture(sprites::longButton, static_cast<int>(screens::middleScreenW) - sprites::longButton.width / 2, static_cast<int>(screens::middleScreenH) + 110, WHITE);
			DrawText("Runner", static_cast<int>(screens::middleScreenW) - 65, static_cast<int>(screens::screenZero) + 80, screens::maxFontSize, WHITE);
			DrawText("Press ESC to exit", static_cast<int>(screens::screenWidth) - 200, static_cast<int>(screens::screenHeight) - 30, screens::minFontSize, WHITE);
			DrawText("1  Start Game", static_cast<int>(screens::middleScreenW) - 100, static_cast<int>(screens::middleScreenH) - 56, screens::medFontSize, WHITE);
			DrawText("2 Instructions", static_cast<int>(screens::middleScreenW) - 103, static_cast<int>(screens::middleScreenH) + 35, screens::medFontSize, WHITE);
			DrawText("3   Credits", static_cast<int>(screens::middleScreenW) - 103, static_cast<int>(screens::middleScreenH) + 124, screens::medFontSize, WHITE);			
			DrawText("v1.0", static_cast<int>(screens::screenZero) + 15, static_cast<int>(screens::screenHeight) - 30, screens::minFontSize, WHITE);
			audio::drawMuteButton(static_cast<int>(screens::middleScreenW) - (sprites::shortButton.width / 2), static_cast<int>(screens::middleScreenH) + 190);
		}
		void deinit()
		{
			sprites::deinit();
			audio::deinit();
		}
	}
}