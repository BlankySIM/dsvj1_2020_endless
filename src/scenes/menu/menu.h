#ifndef MENU_H
#define MENU_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites_manager/sprites.h"
#include "audio_manager/audio.h"

namespace runner
{
	namespace menu
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif

