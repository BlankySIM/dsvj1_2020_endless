#include "results.h"

namespace runner
{
	namespace results
	{
		const int minusc = 96;
		const int mayusc = 32;
		int highestScore = 0;
		bool calculatedScore = false;
		int key;

		static void setHighestScore()
		{
			if (!calculatedScore)
			{
				if (player::runnerPlayer.points > highestScore)
				{
					highestScore = player::runnerPlayer.points;
				}
				calculatedScore = true;
			}
		}
		void init()
		{

		}
		void inputs()
		{
			key = GetKeyPressed();

			if (key > minusc)
			{
				key -= mayusc;
			}
		}
		void update()
		{
			PlayMusicStream(audio::gameplayMusic);
			UpdateMusicStream(audio::gameplayMusic);
			setHighestScore();
	
			switch (key)
			{
			case KEY_ONE:
				screens::currentScreen = screens::GameplayScreen;
				PlaySound(audio::buttonSfx);
				player::init();
				coin::init();
				obstacle::init();
				calculatedScore = false;
				break;
			case KEY_TWO:
				screens::currentScreen = screens::MenuScreen;
				PlaySound(audio::buttonSfx);
				player::init();
				coin::init();
				obstacle::init();
				StopMusicStream(audio::gameplayMusic);
				calculatedScore = false;
				break;
			}	
		}
		void draw()
		{
			sprites::drawBackground();
			DrawTexture(sprites::longButton, static_cast<int>(screens::middleScreenW) - sprites::longButton.width / 2, static_cast<int>(screens::middleScreenH) + 1, WHITE);
			DrawTexture(sprites::longButton, static_cast<int>(screens::middleScreenW) - sprites::longButton.width / 2, static_cast<int>(screens::middleScreenH) + 76, WHITE);
			DrawText("Game Over", static_cast<int>(screens::middleScreenW) - 110, static_cast<int>(screens::screenZero) + 80, screens::maxFontSize, WHITE);
			DrawText("1  Start again", static_cast<int>(screens::middleScreenW) - 100, static_cast<int>(screens::middleScreenH) + 15, screens::medFontSize, WHITE);
			DrawText("2  Go to menu", static_cast<int>(screens::middleScreenW) - 103, static_cast<int>(screens::middleScreenH) + 90, screens::medFontSize, WHITE);
			DrawText(FormatText("Score: %0i", player::runnerPlayer.points), static_cast<int>(screens::screenZero) + 10, static_cast<int>(screens::screenZero) + 10, screens::maxFontSize, WHITE);
			DrawText(FormatText("Highest score: %0i", highestScore), static_cast<int>(screens::screenZero) + 10, static_cast<int>(screens::screenZero) + 55, screens::minFontSize, WHITE);
		}
		void deinit()
		{

		}
	}
}