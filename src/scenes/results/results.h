#ifndef RESULTS_H
#define RESULTS_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "player/player.h"
#include "audio_manager/audio.h"
#include "sprites_manager/sprites.h"

namespace runner
{
	namespace results
	{
		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
	}
}
#endif