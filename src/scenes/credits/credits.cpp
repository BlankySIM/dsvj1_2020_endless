#include "credits.h"

namespace runner
{
	namespace credits
	{
		const int minusc = 96;
		const int mayusc = 32;
		int key;

		void init()
		{

		}
		void inputs()
		{
			key = GetKeyPressed();

			if (key > minusc)
			{
				key -= mayusc;
			}
		}
		void update()
		{
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);
			switch (key)
			{
			case KEY_M:
				screens::currentScreen = screens::MenuScreen;
				PlaySound(audio::buttonSfx);
				break;
			}
		}
		void draw()
		{
			sprites::drawBackground();
			DrawTexture(sprites::shortButton, static_cast<int>(screens::screenZero) + 7, static_cast<int>(screens::screenHeight) - 56, WHITE);
			DrawText("Credits", static_cast<int>(screens::middleScreenW) - 70, static_cast<int>(screens::screenZero) + 10, screens::maxFontSize, WHITE);
			DrawText("M  Menu", static_cast<int>(screens::screenZero) + 28, static_cast<int>(screens::screenHeight) - 42, screens::minFontSize, WHITE);
			DrawText("Game Dev", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 80, screens::medFontSize, WHITE);
			DrawText("- Blanco Juan Simon || blancojuansimon@gmail.com", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 110, screens::minFontSize, WHITE);
			DrawText("Game Art", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 140, screens::medFontSize, WHITE);
			DrawText("- bevouliin.com", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 170, screens::minFontSize, WHITE);
			DrawText("- Mobile Game Graphics || https://itch.io/profile/mobilegamegraphics", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 190, screens::minFontSize, WHITE);
			DrawText("Game Audio", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 220, screens::medFontSize, WHITE);
			DrawText("- https://freesfx.co.uk", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 250, screens::minFontSize, WHITE);
			DrawText("- TinyWorlds || https://opengameart.org/users/tinyworlds", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 270, screens::minFontSize, WHITE);
			DrawText("- Mopz || https://opengameart.org/users/mopz", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 290, screens::minFontSize, WHITE);
			DrawText("Game created with Raylib || https://www.raylib.com", static_cast<int>(screens::screenZero) + 5, static_cast<int>(screens::screenZero) + 360, screens::medFontSize, WHITE);
		}
		void deinit()
		{

		}
	}
}