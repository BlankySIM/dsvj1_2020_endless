#ifndef OBSTACLE_H
#define OBSTACLE_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites_manager/sprites.h"

namespace runner
{	
	namespace obstacle
	{
		const int maxObstacles = 12;
		extern float easyMaxCooldown;
		extern float hardMaxCooldown ;
		extern float maxCooldown;

		struct OBSTACLE
		{
			Rectangle obstacleRec;
			bool active;
			bool breakable;
		};

		void init();
		void generateObstacles();
		void moveObstacles();
		void draw();

		extern OBSTACLE obstacles[maxObstacles];
	}
}
#endif
