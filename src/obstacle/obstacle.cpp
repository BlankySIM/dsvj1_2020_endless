#include "obstacle.h"

namespace runner
{
	namespace obstacle
	{
		float hardMaxCooldown = 250.0f;
		float easyMaxCooldown = 400.0f;
		float maxCooldown = easyMaxCooldown;
		float minCooldown = 150.0f;
		float obstaclesCooldown = maxCooldown;
		float cooldownTimeDecreaser = 295.0f;
		int maxRand = 150;
		int minRand = 0;
		int obstacleTrigger = 55;
		int backGroundRange = static_cast<int>(maxRand / 3);
		int midGroundRange = static_cast<int>(maxRand - (maxRand / 3));

		static void setNewPosition(int currentObstacle, float yPos)
		{
			if (currentObstacle>=maxObstacles/2)
			{
				obstacles[currentObstacle].obstacleRec.y = yPos - sprites::bigObstacleSize;
			}
			else
			{
				obstacles[currentObstacle].obstacleRec.y = yPos - sprites::obstacleSizeY;
			}
		}
		void init()
		{
			for (int i = 0; i < maxObstacles; i++)
			{				
				if (i >= maxObstacles/2)
				{
					obstacles[i].obstacleRec = { screens::screenWidth, screens::gameGroundFront + sprites::bigObstacleSize, sprites::bigObstacleSize, sprites::bigObstacleSize };
					obstacles[i].breakable = false;
				}
				else
				{
					if (i<=maxObstacles/4)
					{
						obstacles[i].breakable = true;
					}
					else
					{
						obstacles[i].breakable = false;
					}
					obstacles[i].obstacleRec = { screens::screenWidth, screens::gameGroundFront + sprites::obstacleSizeY, sprites::obstacleSizeY, sprites::obstacleSizeX };
				}
				obstacles[i].active = false;
			}
		}
		void generateObstacles()
		{
			int randomNumber;
			randomNumber = GetRandomValue(minRand, maxObstacles);
			for (int i = 0; i < maxObstacles; i++)
			{
				if (randomNumber == i && !obstacles[i].active)
				{					
					if (obstaclesCooldown <= 0)
					{
						obstacles[i].active = true;
						randomNumber = GetRandomValue(minRand, maxRand);
						obstaclesCooldown = static_cast<float>(GetRandomValue(static_cast<int>(minCooldown), static_cast<int>(maxCooldown)));
						if (randomNumber <= backGroundRange)
						{
							setNewPosition(i, screens::gameGroundBack);
						}
						else if (randomNumber <= midGroundRange)
						{
							setNewPosition(i, screens::gameGroundMid);
						}
						else
						{
							setNewPosition(i, screens::gameGroundFront);
						}
					}
				}
			}
			obstaclesCooldown -= cooldownTimeDecreaser * GetFrameTime();
		}
		void moveObstacles()
		{
			for (int i = 0; i < maxObstacles; i++)
			{
				if (obstacles[i].active)
				{
					obstacles[i].obstacleRec.x -= sprites::layer1Speed * GetFrameTime();
				}
				if (obstacles[i].obstacleRec.x <= screens::screenZero - obstacles[i].obstacleRec.width)
				{
					obstacles[i].obstacleRec.x = screens::screenWidth;
					obstacles[i].active = false;
				}
			}
		}
		void draw()
		{
			for (int i = 0; i < maxObstacles; i++)
			{
				if (obstacles[i].active)
				{
					if (obstacles[i].breakable)
					{	
						DrawTexture(sprites::weakObstacle, static_cast<int>(obstacles[i].obstacleRec.x), static_cast<int>(obstacles[i].obstacleRec.y), WHITE);
						#if DEBUG
						DrawRectangle(static_cast<int>(obstacles[i].obstacleRec.x), static_cast<int>(obstacles[i].obstacleRec.y), static_cast<int>(obstacles[i].obstacleRec.height), static_cast<int>(obstacles[i].obstacleRec.width), Fade(PINK, 0.7));
						#endif
					}
					else
					{
						if (obstacles[i].obstacleRec.width == sprites::bigObstacleSize)
						{
							DrawTexture(sprites::bigObstacle, static_cast<int>(obstacles[i].obstacleRec.x), static_cast<int>(obstacles[i].obstacleRec.y), WHITE);
						}
						else
						{
							DrawTexture(sprites::obstacle, static_cast<int>(obstacles[i].obstacleRec.x), static_cast<int>(obstacles[i].obstacleRec.y), WHITE);
						}
						#if DEBUG
						DrawRectangle(static_cast<int>(obstacles[i].obstacleRec.x), static_cast<int>(obstacles[i].obstacleRec.y), static_cast<int>(obstacles[i].obstacleRec.height), static_cast<int>(obstacles[i].obstacleRec.width), Fade(BLUE, 0.7));
						#endif
					}					
				}
			}
		}

		OBSTACLE obstacles[maxObstacles] = { 0 };
	}
}