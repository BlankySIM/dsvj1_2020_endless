#include "coin.h"

namespace runner
{
	float coinAirPosition = 70.0f;
	float coinSpeed = 350.0f;
	float maxCooldown = 700.0f;
	float minCooldown = 300.0f;
	float coinsCooldown = maxCooldown;
	float cooldownTimeDecreaser = 275.0f;
	int maxRand = 150;
	int minRand = 0;
	int coinTrigger = 55;
	int backGroundRange = maxRand / 3;
	int midGroundRange = maxRand - (maxRand / 3);

	namespace coin
	{
		static void setNewPosition(int currentCoin, float yPos)
		{
			if (coins[currentCoin].air)
			{
				coins[currentCoin].coinRec.y = (yPos - coinAirPosition) - sprites::coinSize;
			}
			else
			{
				coins[currentCoin].coinRec.y = yPos - sprites::coinSize;
			}
		}
		void init()
		{
			for (int i = 0; i < maxCoins; i++)
			{
				coins[i].coinRec = { screens::screenWidth, screens::gameGroundFront + sprites::coinSize, sprites::coinSize, sprites::coinSize };
				coins[i].active = false;
				if (i >= maxCoins/2)
				{
					coins[i].air = true;
				}
				else
				{
					coins[i].air = false;
				}
			}
		}
		void generateCoins()
		{
			int randomNumber;
			randomNumber = GetRandomValue(minRand, maxCoins);

			for (int i = 0; i < maxCoins; i++)
			{
					if (randomNumber == i && !coins[i].active)
					{
						if (coinsCooldown <= 0)
						{
							coins[i].active = true;
							randomNumber = GetRandomValue(minRand, maxRand);
							coinsCooldown = static_cast<float>(GetRandomValue(static_cast<int>(minCooldown), static_cast<int>(maxCooldown)));
							if (randomNumber <= backGroundRange)
							{
								setNewPosition(i, screens::gameGroundBack);
							}
							else if (randomNumber <= midGroundRange)
							{
								setNewPosition(i, screens::gameGroundMid);
							}
							else
							{
								setNewPosition(i, screens::gameGroundFront);
							}
						}
					}
				}
			coinsCooldown -= cooldownTimeDecreaser * GetFrameTime();
		}
		void moveCoins()
		{
			for (int i = 0; i < maxCoins; i++)
			{
				if (coins[i].active)
				{
					coins[i].coinRec.x -= coinSpeed * GetFrameTime();
				}
				if (coins[i].coinRec.x <= screens::screenZero - sprites::coinSize)
				{
					coins[i].coinRec.x = screens::screenWidth;
					coins[i].active = false;
				}
			}
		}
		void draw()
		{
			for (int i = 0; i < maxCoins; i++)
			{
				if (coins[i].active)
				{
					#if DEBUG
					DrawRectangle(static_cast<int>(coins[i].coinRec.x), static_cast<int>(coins[i].coinRec.y), static_cast<int>(coins[i].coinRec.height), static_cast<int>(coins[i].coinRec.width), YELLOW);
					#endif
					DrawTexture(sprites::coin, static_cast<int>(coins[i].coinRec.x), static_cast<int>(coins[i].coinRec.y), WHITE);
				}
			}
		}

		COIN coins[12] = { 0 };
	}
}