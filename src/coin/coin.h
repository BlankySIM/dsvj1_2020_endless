#ifndef COIN_H
#define COIN_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites_manager/sprites.h"

namespace runner
{
	namespace coin
	{
		const int maxCoins = 12;

		struct COIN
		{
			Rectangle coinRec;
			bool active;
			bool air;
		};

		void init();
		void generateCoins();
		void moveCoins();
		void draw();

		extern COIN coins[12];
	}
}
#endif