#include "game.h"

namespace runner
{
	static void init()
	{
		InitWindow(static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenHeight), "Runner");
		InitAudioDevice();
		SetTargetFPS(screens::targetFPS);
		gameplay::init();
		menu::init();
		credits::init();
		instructions::init();
		results::init();
	}
	static void inputs()
	{
		switch (screens::currentScreen)
		{
		case screens::GameplayScreen:
			gameplay::inputs();
			break;
		case screens::MenuScreen:
			menu::inputs();
			break;
		case screens::CreditsScreen:
			credits::inputs();
			break;
		case screens::InstructionsScreen:
			instructions::inputs();
			break;
		case screens::ResultsScreen:
			results::inputs();
			break;
		}
	}
	static void update()
	{
		switch (screens::currentScreen)
		{
		case screens::GameplayScreen:
			gameplay::update();
			break;
		case screens::MenuScreen:
			menu::update();
			break;
		case screens::CreditsScreen:
			credits::update();
			break;
		case screens::InstructionsScreen:
			instructions::update();
			break;
		case screens::ResultsScreen:
			results::update();
			break;
		}
	}
	static void draw()
	{
		BeginDrawing();
		ClearBackground(BLACK);
		switch (screens::currentScreen)
		{
		case screens::GameplayScreen:
			gameplay::draw();
			break;
		case screens::MenuScreen:
			menu::draw();
			break;
		case screens::CreditsScreen:
			credits::draw();
			break;
		case screens::InstructionsScreen:
			instructions::draw();
			break;
		case screens::ResultsScreen:
			results::draw();
			break;
		}
		EndDrawing();
	}
	static void deinit()
	{
		gameplay::deinit();
		menu::deinit();
		credits::deinit();
		instructions::deinit();
		results::deinit();
		CloseAudioDevice();
		CloseWindow();
	}
	void run()
	{
		runner::init();
		while (!WindowShouldClose())
		{
			runner::inputs();
			runner::update();
			runner::draw();
		}
		runner::deinit();
	}
}