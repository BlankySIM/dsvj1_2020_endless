#include "screens.h"

namespace runner
{
	namespace screens
	{
		const float  screenZero = 0.0f;
		const int  minFontSize = 20;
		const int  medFontSize = 30;
		const int  maxFontSize = 40;
		float screenWidth = 800.0f;
		float screenHeight = 600.0f;
		float middleScreenW = screenWidth / 2.0f;
		float middleScreenH = screenHeight / 2.0f;
		float groundsDistance = 135.0f;
		float gameGroundFront = screenHeight - 60.0f;
		float gameGroundMid = gameGroundFront - groundsDistance;
		float gameGroundBack = gameGroundMid - groundsDistance;

		GameState currentScreen = MenuScreen;
	}
}