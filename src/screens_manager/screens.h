#ifndef SCREENS_H
#define SCREENS_H

namespace runner
{
	namespace screens
	{
		const int targetFPS = 60;
		extern const float screenZero;
		extern const int minFontSize;
		extern const int medFontSize;
		extern const int maxFontSize;
		extern float screenWidth;
		extern float screenHeight;
		extern float middleScreenW;
		extern float middleScreenH;
		extern float gameGroundFront;
		extern float gameGroundMid;
		extern float gameGroundBack;
		extern float groundsDistance;

		enum GameState
		{
			MenuScreen,
			GameplayScreen,
			CreditsScreen,
			InstructionsScreen,
			ResultsScreen,
		};

		extern GameState currentScreen;
	}
}
#endif