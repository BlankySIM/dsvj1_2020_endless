#ifndef SPRITES_H
#define SPRITES_H
#include "raylib.h"
#include "screens_manager/screens.h"

namespace runner
{
	namespace sprites
	{
		extern Texture2D playerRun;
		extern Texture2D playerJump;
		extern Texture2D playerFall;
		extern Texture2D playerAttack;
		extern Texture2D obstacle;
		extern Texture2D bigObstacle;
		extern Texture2D weakObstacle;
		extern Texture2D coin;
		extern Texture2D longButton;
		extern Texture2D shortButton;
		extern float playerH;
		extern float playerW;
		extern float layer1Speed;
		extern float playerAttackW;
		extern float playerAttackH;
		extern float obstacleSizeX;
		extern float obstacleSizeY;
		extern float bigObstacleSize;
		extern float coinSize;
		const int runFrames = 4;

		void init(float newWidth, float newHeight);
		void initParallaxPositions();
		void drawBackground();
		void updateParallax();
		void drawParallaxBackground();
		void drawPlatforms();
		void deinit();
	}
}
#endif