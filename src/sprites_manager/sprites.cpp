#include "sprites.h"

namespace runner
{
	namespace sprites
	{
		Image cityImage1 = LoadImage("res/assets/city_background/layer_1.png");
		Image cityImage2 = LoadImage("res/assets/city_background/layer_2.png");
		Image cityImage3 = LoadImage("res/assets/city_background/layer_3.png");
		Image cityImage4 = LoadImage("res/assets/city_background/layer_4.png");
		Image cityImage5 = LoadImage("res/assets/city_background/layer_5.png");
		Image cityImage6 = LoadImage("res/assets/city_background/layer_6.png");
		Image cityImage7 = LoadImage("res/assets/city_background/layer_7.png");
		Image platformImage = LoadImage("res/assets/platform.png");
		Image playerRunImage = LoadImage("res/assets/player/run/run.png");
		Image playerJumpImage = LoadImage("res/assets/player/jump/jump.png");
		Image playerFallImage = LoadImage("res/assets/player/fall/fall.png");
		Image playerAttackImage = LoadImage("res/assets/player/attack/attack.png");
		Image obstacleImage = LoadImage("res/assets/obstacles/obstacle.png");
		Image bigObstacleImage = LoadImage("res/assets/obstacles/big_obstacle.png");
		Image weakObstacleImage = LoadImage("res/assets/obstacles/weak_obstacle.png");
		Image coinImage = LoadImage("res/assets/coin/coin.png");
		Texture2D cityLayer1;
		Texture2D cityLayer2;
		Texture2D cityLayer3;
		Texture2D cityLayer4;
		Texture2D cityLayer5;
		Texture2D cityLayer6;
		Texture2D cityLayer7;
		Texture2D platform;
		Texture2D playerRun;
		Texture2D playerJump;
		Texture2D playerFall;
		Texture2D playerAttack;
		Texture2D obstacle;
		Texture2D bigObstacle;
		Texture2D weakObstacle;
		Texture2D coin;
		Texture2D longButton;
		Texture2D shortButton;
		float layer1APosition;
		float layer1BPosition;
		float layer2APosition;
		float layer2BPosition;
		float layer3APosition;
		float layer3BPosition;
		float layer4APosition;
		float layer4BPosition;
		float layer5APosition;
		float layer5BPosition;
		float layer1Speed = 315.0f;
		float layer2Speed = 250.0f;
		float layer3Speed = 80.0f;
		float layer4Speed = 60.0f;
		float layer5Speed = 20.0f;
		float playerH = 45.0f;
		float playerW = 35.0f;
		float playerAttackW = 47.0f;
		float playerAttackH = 32.0f;
		float obstacleSizeX = 48.0f;
		float obstacleSizeY = 40.0f;
		float bigObstacleSize = 60.0f;
		float coinSize = 40.0f;
		float runWidth;
		float runHeight;

		static void moveTextureBehind(float &imagePosition, float imageBehindPosition)
		{
			if (imagePosition + cityLayer1.width <= 0)
			{
				imagePosition = imageBehindPosition + cityLayer1.width;
			}
		}
		void init(float newWidth, float newHeight)
		{
			runWidth = playerW * runFrames;
			runHeight = playerH;
			int backgroundW = static_cast<int>(cityImage1.width);
			int backgroundH = static_cast<int>(cityImage1.height);
			int newBackgroundW = static_cast<int>((backgroundW * newHeight) / backgroundH);

			ImageResize(&sprites::cityImage1, newBackgroundW, static_cast<int>(newHeight));
			ImageResize(&sprites::cityImage2, newBackgroundW, static_cast<int>(newHeight));
			ImageResize(&sprites::cityImage3, newBackgroundW, static_cast<int>(newHeight));
			ImageResize(&sprites::cityImage4, newBackgroundW, static_cast<int>(newHeight));
			ImageResize(&sprites::cityImage5, newBackgroundW, static_cast<int>(newHeight));
			ImageResize(&sprites::cityImage6, newBackgroundW, static_cast<int>(newHeight));
			ImageResize(&sprites::cityImage7, newBackgroundW, static_cast<int>(newHeight));
			ImageResize(&sprites::platformImage, static_cast<int>(newWidth), static_cast<int>((platformImage.height*newWidth) / platformImage.width));
			ImageResize(&sprites::playerRunImage, static_cast<int>(runWidth), static_cast<int>(runHeight));
			ImageResize(&sprites::playerJumpImage, static_cast<int>(playerW), static_cast<int>(playerH));
			ImageResize(&sprites::playerFallImage, static_cast<int>(playerW), static_cast<int>(playerH));
			ImageResize(&sprites::playerAttackImage, static_cast<int>(playerAttackW), static_cast<int>(playerAttackH));
			ImageResize(&sprites::obstacleImage, static_cast<int>(obstacleSizeX), static_cast<int>(obstacleSizeY));
			ImageResize(&sprites::weakObstacleImage, static_cast<int>(obstacleSizeX), static_cast<int>(obstacleSizeY));
			ImageResize(&sprites::bigObstacleImage, static_cast<int>(bigObstacleSize), static_cast<int>(bigObstacleSize));
			ImageResize(&sprites::coinImage, static_cast<int>(coinSize), static_cast<int>(coinSize));
			cityLayer1 = LoadTextureFromImage(cityImage1);
			cityLayer2 = LoadTextureFromImage(cityImage2);
			cityLayer3 = LoadTextureFromImage(cityImage3);
			cityLayer4 = LoadTextureFromImage(cityImage4);
			cityLayer5 = LoadTextureFromImage(cityImage5);
			cityLayer6 = LoadTextureFromImage(cityImage6);
			cityLayer7 = LoadTextureFromImage(cityImage7);
			platform = LoadTextureFromImage(platformImage);
			playerRun = LoadTextureFromImage(playerRunImage);
			playerJump = LoadTextureFromImage(playerJumpImage);
			playerFall = LoadTextureFromImage(playerFallImage);
			playerAttack = LoadTextureFromImage(playerAttackImage);
			obstacle = LoadTextureFromImage(obstacleImage);
			weakObstacle = LoadTextureFromImage(weakObstacleImage);
			bigObstacle = LoadTextureFromImage(bigObstacleImage);
			coin = LoadTextureFromImage(coinImage);
			longButton = LoadTexture("res/assets/buttons/long_button.png");
			shortButton = LoadTexture("res/assets/buttons/short_button.png");
		}
		void initParallaxPositions()
		{
			layer1APosition = screens::screenZero;
			layer2APosition = screens::screenZero;
			layer3APosition = screens::screenZero;
			layer4APosition = screens::screenZero;
			layer5APosition = screens::screenZero;
			layer1BPosition = screens::screenZero + cityLayer1.width;
			layer2BPosition = screens::screenZero + cityLayer2.width;
			layer3BPosition = screens::screenZero + cityLayer3.width;
			layer4BPosition = screens::screenZero + cityLayer4.width;
			layer5BPosition = screens::screenZero + cityLayer5.width;
		}
		void drawBackground()
		{		
			DrawTexture(cityLayer7, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero), WHITE);
			DrawTexture(cityLayer6, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero), WHITE);
			DrawTexture(cityLayer5, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero+72), WHITE);
			DrawTexture(cityLayer4, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero+72), WHITE);
			DrawTexture(cityLayer3, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero+72), WHITE);
			DrawTexture(cityLayer2, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero+72), WHITE);
			DrawTexture(cityLayer1, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero+72), WHITE);
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero), static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenHeight), Fade(BLACK, 0.3f));
		}
		void updateParallax()
		{
			layer1APosition -= layer1Speed * GetFrameTime();
			layer1BPosition -= layer1Speed * GetFrameTime();
			layer2APosition -= layer2Speed * GetFrameTime();
			layer2BPosition -= layer2Speed * GetFrameTime();
			layer3APosition -= layer3Speed * GetFrameTime();
			layer3BPosition -= layer3Speed * GetFrameTime();
			layer4APosition -= layer4Speed * GetFrameTime();
			layer4BPosition -= layer4Speed * GetFrameTime();
			layer5APosition -= layer5Speed * GetFrameTime();
			layer5BPosition -= layer5Speed * GetFrameTime();
			moveTextureBehind(layer1APosition, layer1BPosition);
			moveTextureBehind(layer1BPosition, layer1APosition);
			moveTextureBehind(layer2APosition, layer2BPosition);
			moveTextureBehind(layer2BPosition, layer2APosition);
			moveTextureBehind(layer3APosition, layer3BPosition);
			moveTextureBehind(layer3BPosition, layer3APosition);
			moveTextureBehind(layer4APosition, layer4BPosition);
			moveTextureBehind(layer4BPosition, layer4APosition);
			moveTextureBehind(layer5APosition, layer5BPosition);
			moveTextureBehind(layer5BPosition, layer5APosition);
		}
		void drawParallaxBackground()
		{
			DrawTexture(cityLayer7, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero), WHITE);
			DrawTexture(cityLayer6, static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero), WHITE);
			DrawTexture(cityLayer5, static_cast<int>(layer5APosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer5, static_cast<int>(layer5BPosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer4, static_cast<int>(layer4APosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer4, static_cast<int>(layer4BPosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer3, static_cast<int>(layer3APosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer3, static_cast<int>(layer3BPosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer2, static_cast<int>(layer2APosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer2, static_cast<int>(layer2BPosition), static_cast<int>(screens::screenZero + 72), WHITE);
			DrawTexture(cityLayer1, static_cast<int>(layer1APosition), static_cast<int>(screens::screenZero + 72), WHITE);			
			DrawTexture(cityLayer1, static_cast<int>(layer1BPosition), static_cast<int>(screens::screenZero + 72), WHITE);
		}
		void drawPlatforms()
		{
			DrawTexture(platform, static_cast<int>(screens::screenZero), static_cast<int>(screens::gameGroundMid), WHITE);
			DrawTexture(platform, static_cast<int>(screens::screenZero), static_cast<int>(screens::gameGroundBack), WHITE);
		}
		void deinit()
		{
			UnloadImage(cityImage1);
			UnloadImage(cityImage2);
			UnloadImage(cityImage3);
			UnloadImage(cityImage4);
			UnloadImage(cityImage5);
			UnloadImage(cityImage6);
			UnloadImage(cityImage7);
			UnloadImage(playerRunImage);
			UnloadImage(playerJumpImage);
			UnloadImage(playerFallImage);
			UnloadImage(playerAttackImage);
			UnloadImage(platformImage);
			UnloadImage(obstacleImage);
			UnloadImage(weakObstacleImage);
			UnloadImage(bigObstacleImage);
			UnloadImage(coinImage);
			UnloadTexture(cityLayer1);
			UnloadTexture(cityLayer2);
			UnloadTexture(cityLayer3);
			UnloadTexture(cityLayer4);
			UnloadTexture(cityLayer5);
			UnloadTexture(cityLayer6);
			UnloadTexture(cityLayer7);
			UnloadTexture(platform);
			UnloadTexture(playerRun);
			UnloadTexture(playerJump);
			UnloadTexture(playerFall);
			UnloadTexture(playerAttack);
			UnloadTexture(obstacle);
			UnloadTexture(weakObstacle);
			UnloadTexture(bigObstacle);
			UnloadTexture(coin);
			UnloadTexture(longButton);
			UnloadTexture(shortButton);
		}
	}
}