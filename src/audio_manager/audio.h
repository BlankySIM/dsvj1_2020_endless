#ifndef AUDIO_H
#define AUDIO_H
#include "raylib.h"
#include "sprites_manager/sprites.h"

namespace runner
{
	namespace audio
	{
		extern Music menuMusic;
		extern Music gameplayMusic;
		extern Sound buttonSfx;
		extern Sound coinSfx;
		extern Sound jumpSfx;
		extern Sound hitSfx;
		const float gameplayMusicVolume = 0.6f;
		const float pauseVolume = 0.2f;
		extern bool muted;

		void muteInput();
		void setVolumes();
		void drawMuteButton(int xPos, int yPos);
		void deinit();
	}
}
#endif