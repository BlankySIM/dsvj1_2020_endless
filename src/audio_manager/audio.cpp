#include "audio.h"

namespace runner
{
	namespace audio
	{
		Music menuMusic = LoadMusicStream("res/assets/audio/music/menu_music.mp3");
		Music gameplayMusic = LoadMusicStream("res/assets/audio/music/gameplay_music.mp3");
		Sound buttonSfx = LoadSound("res/assets/audio/sfx/button_sfx.mp3");
		Sound coinSfx = LoadSound("res/assets/audio/sfx/coin_sfx.mp3");
		Sound jumpSfx = LoadSound("res/assets/audio/sfx/player_jump_sfx.mp3");
		Sound hitSfx = LoadSound("res/assets/audio/sfx/hit_sfx.mp3");
		const float menuMusicVolume = 0.3f;
		const float buttonSfxVolume = 0.8f;
		const float coinSfxVolume = 0.08f;
		const float jumpSfxVolume = 0.1f;
		const float hitSfxVolume = 0.2f;
		const float mutedVolume = 0.0f;
		bool muted = false;

		static void muteVolumes()
		{
			SetMusicVolume(menuMusic, mutedVolume);
			SetMusicVolume(gameplayMusic, mutedVolume);
			SetSoundVolume(buttonSfx, mutedVolume);
			SetSoundVolume(coinSfx, mutedVolume);
			SetSoundVolume(jumpSfx, mutedVolume);
			SetSoundVolume(hitSfx, mutedVolume);
		}
		void muteInput()
		{
			if (IsKeyPressed(KEY_L))
			{
				if (!muted)
				{
					muted = true;
					muteVolumes();
				}
				else
				{
					muted = false;
					setVolumes();
				}
			}
		}
		void setVolumes()
		{
			SetMusicVolume(menuMusic, menuMusicVolume);
			SetMusicVolume(gameplayMusic, gameplayMusicVolume);
			SetSoundVolume(buttonSfx, buttonSfxVolume);
			SetSoundVolume(coinSfx, coinSfxVolume);
			SetSoundVolume(jumpSfx, jumpSfxVolume);
			SetSoundVolume(hitSfx, hitSfxVolume);
		}
		void drawMuteButton(int xPos, int yPos)
		{
			DrawTexture(sprites::shortButton, xPos, yPos, WHITE);
			if (muted)
			{
				DrawText("L  Unmute", xPos + 24, yPos + 14, screens::minFontSize, WHITE);
			}
			else
			{
				DrawText("L    Mute", xPos + 24, yPos + 14, screens::minFontSize, WHITE);
			}
		}
		void deinit()
		{
			UnloadMusicStream(menuMusic);
			UnloadMusicStream(gameplayMusic);
			UnloadSound(buttonSfx);
			UnloadSound(coinSfx);
			UnloadSound(jumpSfx);
			UnloadSound(hitSfx);
		}
	}
}