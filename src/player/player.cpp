#include "player.h"

namespace runner
{
	namespace player
	{
		const float defJumpSpeed = -300.0f;
		const float defFallSpeed = 170.0f;
		float maxJumpH = 80.0f;
		const float attackTime = 50.0f;
		const float attackTimeDecreaser = 150.0f;
		const int minusc = 96;
		const int mayusc = 32;
		float currentJumpH;
		float currentGroundH;
		float attackCooldown = attackTime;
		int key;
		Rectangle frameRec;
		Vector2 playerTexturePos;
		int framesCounter = 0;
		int frameSpeed = 10;
		int currentFrame = 0;
		const int maxPoints = 999999999;
		const int coinPoints = 4;
		const int obstaclePoints = 1;

		static void jump()
		{
			if (runnerPlayer.collisionRec.y >= currentJumpH && !runnerPlayer.falling)
			{
				runnerPlayer.collisionRec.y += runnerPlayer.jumpSpeed * GetFrameTime();
			}
			else
			{
				runnerPlayer.falling = true;
				runnerPlayer.collisionRec.y += runnerPlayer.fallSpeed * GetFrameTime();
				if (runnerPlayer.collisionRec.y >= currentGroundH)
				{
					runnerPlayer.jumping = false;
					runnerPlayer.falling = false;
					runnerPlayer.collisionRec.y = currentGroundH;
				}
			}
		}
		static void coinCollision()
		{
			for (int i = 0; i < coin::maxCoins; i++)
			{
				if (CheckCollisionRecs(coin::coins[i].coinRec, runnerPlayer.collisionRec))
				{
					runnerPlayer.points += coinPoints;
					coin::coins[i].active = false;
					coin::coins[i].coinRec.x = screens::screenWidth;
					PlaySound(audio::coinSfx);
				}
			}
		}
		static void obstacleCollision()
		{
			for (int i = 0; i < obstacle::maxObstacles; i++)
			{
				if (CheckCollisionRecs(obstacle::obstacles[i].obstacleRec, runnerPlayer.collisionRec))
				{
					if (runnerPlayer.attack && obstacle::obstacles[i].breakable)
					{
						runnerPlayer.points += obstaclePoints;
						obstacle::obstacles[i].active = false;
						obstacle::obstacles[i].obstacleRec.x = screens::screenWidth;
					}
					else
					{
						runnerPlayer.alive = false;
					}
					PlaySound(audio::hitSfx);
				}
			}
		}
		void init()
		{	
			runnerPlayer.collisionRec.height = sprites::playerH;
			runnerPlayer.collisionRec.width = sprites::playerW;
			runnerPlayer.collisionRec.x = screens::screenZero + 60;
			runnerPlayer.collisionRec.y = screens::gameGroundFront - runnerPlayer.collisionRec.height;
			runnerPlayer.points = 0;
			runnerPlayer.alive = true;
			runnerPlayer.jumping = false;
			runnerPlayer.falling = false;
			runnerPlayer.attack = false;
			runnerPlayer.jumpSpeed = defJumpSpeed;
			runnerPlayer.fallSpeed = defFallSpeed;
			currentJumpH = runnerPlayer.collisionRec.y - maxJumpH;
			currentGroundH = runnerPlayer.collisionRec.y;
			frameRec.height = runnerPlayer.collisionRec.height;
			frameRec.width = runnerPlayer.collisionRec.width;
		}
		void inputs()
		{
			key = GetKeyPressed();

			if (key > minusc)
			{
				key -= mayusc;
			}
		}
		static void updatePlayerFrames()
		{
			framesCounter++;

			if (framesCounter >= (screens::targetFPS / frameSpeed))
			{
				framesCounter = 0;
				currentFrame++;
				if (currentFrame >= sprites::runFrames) currentFrame = 0;

				frameRec.x = static_cast<float>(currentFrame) * (sprites::playerRun.width / sprites::runFrames);
			}
		}
		void update()
		{
			switch (key)
			{
			case KEY_Q:
				if (runnerPlayer.collisionRec.y != screens::gameGroundBack- runnerPlayer.collisionRec.height && !runnerPlayer.jumping && !runnerPlayer.attack)
				{
					runnerPlayer.collisionRec.y -= screens::groundsDistance;
					currentJumpH = runnerPlayer.collisionRec.y - maxJumpH;
					currentGroundH = runnerPlayer.collisionRec.y;
				}
				break;
			case KEY_A:
				if (runnerPlayer.collisionRec.y != screens::gameGroundFront - runnerPlayer.collisionRec.height && !runnerPlayer.jumping && !runnerPlayer.attack)
				{
					runnerPlayer.collisionRec.y += screens::groundsDistance;
					currentJumpH = runnerPlayer.collisionRec.y - maxJumpH;
					currentGroundH = runnerPlayer.collisionRec.y;
				}
				break;
			case KEY_K:
				if (!runnerPlayer.jumping && !runnerPlayer.attack)
				{
					runnerPlayer.jumping = true;
					PlaySound(audio::jumpSfx);
				}
				break;
			case KEY_L:
				if (!runnerPlayer.attack && !runnerPlayer.jumping)
				{
					runnerPlayer.attack = true;
					runnerPlayer.collisionRec.height = sprites::playerAttackH;
					runnerPlayer.collisionRec.width = sprites::playerAttackW;
					runnerPlayer.collisionRec.y = (runnerPlayer.collisionRec.y + sprites::playerH) - sprites::playerAttackH;
				}
				break;
			}
			if (runnerPlayer.jumping)
			{
				jump();
			}
			if (runnerPlayer.attack)
			{
				attackCooldown -= attackTimeDecreaser * GetFrameTime();
				if (attackCooldown<=0)
				{
					attackCooldown = attackTime;
					runnerPlayer.collisionRec.height = sprites::playerH;
					runnerPlayer.collisionRec.width = sprites::playerW;
					runnerPlayer.collisionRec.y = (runnerPlayer.collisionRec.y + sprites::playerAttackH) - sprites::playerH;
					runnerPlayer.attack = false;
				}
			}
			coinCollision();
			obstacleCollision();
			playerTexturePos.x = runnerPlayer.collisionRec.x;
			playerTexturePos.y = runnerPlayer.collisionRec.y;
			updatePlayerFrames();
			if (runnerPlayer.points > maxPoints)
			{
				runnerPlayer.points = maxPoints;
			}
		}
		void draw()
		{
			#if DEBUG
			if (!runnerPlayer.attack)
			{
				DrawRectangle(static_cast<int>(runnerPlayer.collisionRec.x), static_cast<int>(runnerPlayer.collisionRec.y), static_cast<int>(runnerPlayer.collisionRec.width), static_cast<int>(runnerPlayer.collisionRec.height), RED);
			}
			else
			{
				DrawRectangle(static_cast<int>(runnerPlayer.collisionRec.x), static_cast<int>(runnerPlayer.collisionRec.y), static_cast<int>(runnerPlayer.collisionRec.width), static_cast<int>(runnerPlayer.collisionRec.height), GREEN);
			}
			#endif
			if (runnerPlayer.attack)
			{				
				DrawTexture(sprites::playerAttack, static_cast<int>(runnerPlayer.collisionRec.x), static_cast<int>(runnerPlayer.collisionRec.y), WHITE);
			}
			else if (runnerPlayer.falling)
			{
				DrawTexture(sprites::playerFall, static_cast<int>(runnerPlayer.collisionRec.x), static_cast<int>(runnerPlayer.collisionRec.y), WHITE);
			}
			else if (runnerPlayer.jumping)
			{		
				DrawTexture(sprites::playerJump, static_cast<int>(runnerPlayer.collisionRec.x), static_cast<int>(runnerPlayer.collisionRec.y), WHITE);
			}
			else
			{					
				DrawTextureRec(sprites::playerRun, frameRec, playerTexturePos, WHITE);
			}
			DrawText(FormatText("Score: %0i", runnerPlayer.points), static_cast<int>(screens::screenZero) + 10, static_cast<int>(screens::screenZero) + 10, screens::maxFontSize, WHITE);
		}

		PLAYER runnerPlayer = { 0 };
	}
}