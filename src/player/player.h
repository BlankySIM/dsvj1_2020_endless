#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "coin/coin.h"
#include "obstacle/obstacle.h"
#include "sprites_manager/sprites.h"
#include "audio_manager/audio.h"

namespace runner
{
	namespace player
	{
		struct PLAYER
		{
			Rectangle collisionRec;
			int points;
			bool alive;
			float jumpSpeed;
			float fallSpeed;
			bool jumping;
			bool falling;
			bool attack;
		};

		void init();
		void inputs();
		void update();
		void draw();

		extern PLAYER runnerPlayer;
	}
}
#endif
